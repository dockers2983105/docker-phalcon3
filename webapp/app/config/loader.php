<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'Rmi_module\Models' => APP_PATH . '/common/models/',
    'Rmi_module'        => APP_PATH . '/common/library/',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'Rmi_module\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'Rmi_module\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();
